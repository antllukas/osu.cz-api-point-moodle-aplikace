<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Course;

class CourseController extends Controller
{
    public function index() {
        return Course::all();
    }

    public function create() {
        // TODO: Create logic for posting courses from Nette application
        return ['message' => 'Created.'];
    }
}
